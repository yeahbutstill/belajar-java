# Belajar Java

Perintah-perintah command line Windows

* dir : melihat isi folder
* cls : membersihkan layar command prompt

## Sesi 1 ##

* Compile

    ```
    javac <Nama File>
    ```

    contohnya

    ```
    javac Halo.java
    ```

* Run

    ```
    java <Nama Class>
    ```

    contohnya

    ```
    java Halo
    ```

* Classpath : lokasi di mana JVM mencari class yang ingin digunakan

   * Cara setting classpath di Windows

       ```
       set CLASSPATH="C:\Training Java\belajar-java"
       ```
    
    * Cara setting classpath di Mac

        ```
        export CLASSPATH=/Users/endymuhardin/tmp
        ```

* Package : pengelompokan class dalam Java

    * Dilakukan dengan deklarasi `package namapackage;` di baris pertama source code
    * Untuk menggunakan class dalam package, harus import dulu

        ```java
        import namapackage.NamaClass;
        ```

    * Compile dengan opsi `-d` supaya `javac` membuatkan struktur folder sesuai nama package

* Packaging : menggabungkan seluruh class dan folder menjadi satu file. Caranya dengan membuat file zip berisi class dan folder hasil compile.

    * melihat isi file jar 

        ```
        jar -tvf <nama file>
        ```

        contoh

        ```
        jar -tvf training.jar
        ```
    
    * **PENTING !!!** : file class dan folder package dalam jar harus di top level, tidak boleh dalam subfolder

* Struktur Aplikasi Java

