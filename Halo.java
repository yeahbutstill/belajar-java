import belajar.Komputer;

public class Halo {
    public static void main(String[] args) {
        System.out.println("Training Java Fundamental");

        Komputer k = new Komputer();
        k.merek = "Dell";
        k.tipe = "Inspiron";

        System.out.println("Merek komputer : "+k.merek);
        System.out.println("Tipe komputer : "+k.tipe);
    }
}